import React, { useContext, useEffect } from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';
import authContext from '../context/auth/authContext';
import appContext from '../context/app/appContext';


const Header = () => {

    const router = useRouter();

    const AuthContext = useContext(authContext);
    const AppContext = useContext(appContext);

    const { usuarioAutenticado, usuario, cerrarSession,token } = AuthContext;
    const { limpiarState } = AppContext;


    useEffect(() => {
        usuarioAutenticado();
    }, []);

    const redireccionar = () => {
        router.push('/');
        limpiarState(); 
    }

    // si agrego src=logo.svg se vera el logo en las paginas principales, pero si usamos routing dinamico, se debe agregar la barra lateral para agregarla como URL abosoluta
    return (
        <header className="py-8 flex flex-col md:flex-row items-center justify-between">
            <img onClick={() => redireccionar()} className="w-64 mb-8 md:mb-0 cursor-pointer" src="/logo.svg" />
            <div>

                {usuario && token ? (
                    <>
                        <div className="flex items-center">
                            <p className="mr-2">Hola {usuario.nombre}</p>
                            <button className="bg-black px-500 py-3 rounded-lg text-white font-bold uppercase" type="button" onClick={() => cerrarSession()}>Cerrar Sesion</button>
                        </div>
                    </>) : (
                        <>
                            <Link href="/login">
                                <a className="bg-red-500 px-500 py-3 rounded-lg text-white font-bold uppercase mr-2">Iniciar Sesion</a>
                            </Link>
                            <Link href="/crearcuenta">
                                <a className="bg-black px-500 py-3 rounded-lg text-white font-bold uppercase">Crear Cuenta</a>
                            </Link>
                        </>
                    )}


            </div>
        </header>
    );
};

export default Header;