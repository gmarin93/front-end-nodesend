import React,{useState,useContext} from 'react';
import appContext from '../context/app/appContext';


const Formulario = () => {

    const AppContext = useContext(appContext);
    const { agregarPassword,agregarDescargas } = AppContext;



    const [tienePassword,setTienePassword] = useState(false);

    return (
        <div className="w-full mt-20 ">
            <div>
                <label className="text-lg text-gray-800">Eliminar tras: </label>
                <select onChange={e=>agregarDescargas(parseInt(e.target.value))}  className="appeareance-none w-full mt-2 bg-white border border-gray-400 text-black py-3 px-4 pr-8 rounded leading:none focus:outline-none border-gray-500">
                    <option value="" selected disabled>-- Seleccione --</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                </select>
            </div>
            <div className="mt-4">
                <div className="flex justify-between items-center">
                    <label className="text-lg text-gray-800">Proteger con Contrasena</label>
                    <input type="checkbox" onChange={()=>setTienePassword(!tienePassword)}/>
                </div>
                {tienePassword ? (
                    <input type="password" onChange={e=>agregarPassword(e.target.value)} className="w-full mt-2 bg-white border border-gray-400 text-black py-3 px-4 pr-8 rounded leading:none focus:outline-none border-gray-500" />
                ):null}
            </div>
        </div>
    );
};

export default Formulario;