import clienteAxios from './axios';

const tokenAuth = token =>{
    if(token){
        //De esta forma se carga en la cabeceras, el token para realizar consultas
        clienteAxios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
    else{
        delete clienteAxios.defaults.headers.common['Authorization'];
    }
};

export default tokenAuth;