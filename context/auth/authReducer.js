import { USUARIO_AUTENTICADO, CERRAR_SESION,REGISTRO_EXITOSO, REGISTRO_ERRONEO, LIMPIAR_ALERTA, LOGIN_EXITOSO, LOGIN_ERRONEO  } from '../../types';

export default(state,action) =>{
    switch(action.type){

        case USUARIO_AUTENTICADO:{
            return{
                ...state,
                usuario:action.payload,
                autenticado:true
            }
        }
        case REGISTRO_EXITOSO:{
            return{
                ...state,
                mensaje:action.payload
            }
        }
        case REGISTRO_ERRONEO:{
            return{
                ...state,
                mensaje:action.payload
            }
        }
        case LIMPIAR_ALERTA:{
            return{
                ...state,
                mensaje:null
            }
        }
        case LOGIN_EXITOSO:{
            localStorage.setItem('rns_token',action.payload);
            return{
                ...state,
                autenticado:true,
                token:action.payload
            }
        }
        case LOGIN_ERRONEO:{
            return{
                ...state,
                mensaje:action.payload
            }
        }
        case CERRAR_SESION:{
            localStorage.removeItem('rns_token');
            return{
                ...state,
                usuario:null,
                token:null,
                autenticado:null
            }
        }

        default:
            return state;
    }
}