import AuthContext from './authContext';
import React, { useReducer } from 'react';
import authReducer from './authReducer';
import { USUARIO_AUTENTICADO, CERRAR_SESION, REGISTRO_EXITOSO, REGISTRO_ERRONEO, LIMPIAR_ALERTA, LOGIN_EXITOSO, LOGIN_ERRONEO  } from '../../types';

import clienteAxios from '../../config/axios';
import tokenAuth from '../../config/auth';

const AuthState = ({ children }) => {


    const initialState = {
        // typeof window(Posee todo lo del cliente)
        //Lo que pasa es que next js es hibrido, corre la aplicacion tanto en el cliente como en el servidor
        //Un simple localstorage no se puede trabajar con next, con create react app si, de esta forma trabajamos con el token
        //y localstorage
        token: typeof window !== 'undefined' ? localStorage.getItem('rns_token') : '',
        autenticado: null,
        usuario: null,
        mensaje: null
    }

    const [state, dispatch] = useReducer(authReducer, initialState);

    const registrarUsuario = async datos => {

        try {
            const respuesta = await clienteAxios.post('/api/usuarios', datos);
            dispatch({
                type: REGISTRO_EXITOSO,
                payload: respuesta.data.msg
            })

        } catch (error) {
            dispatch({
                type: REGISTRO_ERRONEO,
                payload: error.response.data.msg
            })
        }

        //Limpia la alerta despues de 3 seg
        setTimeout(() => {
            dispatch({
                type: LIMPIAR_ALERTA
            })
        }, 3000)

    }

    const iniciarSesion = async datos => {
        try {
            const respuesta = await clienteAxios.post('/api/auth', datos);
            dispatch({
                type: LOGIN_EXITOSO,
                payload:respuesta.data.token
            })
        
        } catch (error) {
            dispatch({
                type: LOGIN_ERRONEO,
                payload: error.response.data.msg
            })
        }

        //Limpia la alerta despues de 3 seg
        setTimeout(() => {
            dispatch({
                type: LIMPIAR_ALERTA
            })
        }, 3000)

    }

    const usuarioAutenticado = async () => {

        const token = localStorage.getItem('rns_token');

        if(token){
            tokenAuth(token);
        }

        try {
            const respuesta = await clienteAxios.get('/api/auth');

            if(respuesta.data.usuario){
                dispatch({
                    type:USUARIO_AUTENTICADO,
                    payload:respuesta.data.usuario
                });
            }
        } catch (error) {
            console.log(error);
        }
    }

    const cerrarSession = async() =>{
        dispatch({
            type:CERRAR_SESION,
        });  
    };
    


    return (
        <AuthContext.Provider
            value={{
                token: state.token,
                usuario: state.usuario,
                autenticado: state.autenticado,
                mensaje: state.mensaje,
                usuarioAutenticado,
                registrarUsuario,
                iniciarSesion,
                cerrarSession
            }}
        >
            {children}
        </AuthContext.Provider>
    )
};

export default AuthState;